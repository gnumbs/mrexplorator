import settings
from settings import *
import interfaceManager
from interfaceManager import *
import fileManager
from fileManager import *
import executionManager
from executionManager import *
import argparse
import sys

################## ARGUMENT PARSING ##################

parser = argparse.ArgumentParser(description='Synthesize the input circuit on selected memristor-based architecture.')
parser.add_argument('-a', '--arch', help="architecture to use for synthesis")
parser.add_argument('-i', '--input', help="input file to be processed")
parser.add_argument('-t', '--toolset', help="force MRExp to use the specified toolset for conversion")
# parser.add_argument('-u', '--opt', help="run the synthesis process using optional parameters")
args = parser.parse_args()
filename, file_extension = os.path.splitext(args.input)

################## CHECKS ##################

interfaceInit()
arch = args.arch.lower()
if (not checkArch(arch) and arch != "all"):
    print(bcolors.FAIL+"[ERROR] Invalid architecture. Please choose one (or all) of the architectures listed above.")
    sys.exit(1)
if (arch != "all"): #Targetting a specific architecture
    neededFormat = checkInputFormat(arch)
    if (file_extension[1:].lower() not in neededFormat): #CONVERSION NEEDED
        if (args.toolset is None): #No toolset specified by user in the arguments
            for nF in neededFormat:
                print(bcolors.HEADER+"[INFO] Searching for a toolset able to manage file conversion from ."+file_extension[1:]+" to ."+nF+"...")
                convTool = checkConversion(file_extension[1:], nF, "")
                if (convTool):
                    targetFormat = nF
                    break
        else: #User wants to use a specific toolset
            toolsetList = listdir(toolsetPath)
            if (args.toolset.lower() in toolsetList): #The specified toolset is available
                for nF in neededFormat:
                    convTool = checkConversion(file_extension[1:], nF, args.toolset.lower())
                    if (convTool):
                        targetFormat = nF
                        break
            else: #The specified toolset is NOT available
                print(bcolors.WARNING+"[WARNING] The choosen toolset is not installed.")
                for nF in neededFormat:
                    print(bcolors.HEADER+"[INFO] Searching for a toolset able to manage file conversion from ."+file_extension[1:]+" to ."+nF+"...")
                    convTool = checkConversion(file_extension[1:], nF, "")
                    if (convTool):
                        targetFormat = nF
                        break
        try: #No toolset can manage this conversion
            convTool
        except NameError:
            print(bcolors.FAIL+"[ERROR] No toolset can convert ."+file_extension[1:]+" to ."+neededFormat)
            sys.exit(1)
    else: targetFormat = file_extension[1:].lower() #NO CONVERSION NEEDED
else: #Targetting all the available architectures
    print("Targetting all the architectures!")
    archList = listdir(archsPath)
    if (archList):
        for a in archList:
            neededFormat = checkInputFormat(a)
    sys.exit(0)

################## CONVERSION ##################

try:
    convTool
except NameError:
    print(bcolors.HEADER+"[INFO] No conversion needed. OK")
else:
    convert(filename, file_extension[1:], targetFormat, convTool)

################## SYNTHESIS ##################

synth(arch, filename+"."+targetFormat, True)
