# MR Explorator - Memristor Architectures Explorator

MR Explorator is a simple & flexible & **padulo** Python tool for exploring and synthesizing circuits on different memristor-based architectures.  
Currently, supported mr-based architectures are FBLC/SFBLC, Magic, IMPLY Logic through the corresponding tools: XbarGen, Simple, IMPLY Compiler...  
But adding new architectures, it's less than a moment!

## Getting started

Actually there is no more content here.  
Just check the **MRExplorator.pdf** file in the repo for software design specifications. If you know what i mean.

### Prerequisites

### Installing

## Authors

* **Umberto Ferrandino** 

## License

* Your **sister** can use this software for free.

## Acknowledgments

* Stackoverflow, as always.

 
