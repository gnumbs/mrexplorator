import settings
from settings import *
from os import listdir
from os.path import isfile, join
import os
import xml.etree.ElementTree as ET

def interfaceInit():
    print("Welcome in MR Explorator by Umberto Ferrandino") 
    print("Checking supported Architectures...")
    archs = [f for f in listdir(archsPath) if isfile(join(archsPath, f))]
    toolsetList = listdir(toolsetPath) 
    for i in archs:
        i = os.path.splitext(i)[0]
        archList.append(i)
    print(archList)
    print("Checking toolset...")
    print(toolsetList)
    if (len(archs) == 0 or len(toolsetList) == 0):
        print("No architectures or toolset found! Please check that archs and toolset direcotry are not empty.")

def checkArch(arch):
    if (arch in archList or arch.upper() in archList or arch.lower() in archList):
        return True

def checkInputFormat(arch):
    arch = arch.lower()
    tree = ET.parse(archsPath+'/'+arch+'.xml')
    root = tree.getroot()
    formatIn = root.find('input').text.lower().split(',')
    return formatIn


