import settings
from settings import *
from os import listdir
from os.path import isfile, join
import xml.etree.ElementTree as ET
import os
import subprocess
from subprocess import call
import string
import sys
import re

def convert(filename, inputFormat, outputFormat, toolset):
    cfg = open(toolsetPath+'/'+toolset+'/'+toolset+'.cfg')
    cfgLines = cfg.readlines()
    rc = open(toolsetPath+'/'+toolset+'/cp/'+inputFormat+'-'+outputFormat+'.rc')
    rcTemp = open(toolsetPath+'/'+toolset+'/cp/'+inputFormat+'-'+outputFormat+'_exec.rc', 'w')
    rcLines = rc.readlines()
    cmd = cfgLines[0].split(':')[1].replace("\n", "")
    cmdPar = cfgLines[2].split(':')[1].replace("\n", "")
    # Command parameters mapping
    try:
        cmdPar = cmdPar.format(command=toolsetPath+'/'+toolset+'/cp/'+inputFormat+'-'+outputFormat+'_exec.rc', input=filename+'.'+inputFormat, output=filename+'.'+outputFormat)
        cmdExec = cmd + ' ' + cmdPar
    except KeyError:
        print (bcolors.FAIL+"[ERROR] Oops! Wrong parameters mapping. Check your .cfg file, needed parameters are {command|input|output}.")
        sys.exit(1)
    # Conversion path file parameters mapping
    for rcLine in rcLines:
        if (re.findall(r'{input}',rcLine)):
            rcLine = rcLine.format(input=filename+'.'+inputFormat)
        elif (re.findall(r'{output}',rcLine)):
            rcLine = rcLine.format(output=filename+'.'+outputFormat)
        rcTemp.write(rcLine)
    rcTemp.close()
    print (bcolors.ENDC+"[EXEC] " + cmdExec)
    subprocess.call(["pwd"])
    subprocess.call([cmd, cmdPar])
    # os.remove(toolsetPath+'/'+toolset+'/cp/'+inputFormat+'-'+outputFormat+'_exec.rc')

def synth(arch, inputFile, opt):
    tree = ET.parse(archsPath+'/'+arch+'.xml')
    root = tree.getroot()
    cmd = root.find("cmd").text
    cmd += " " + root.find("basicPar").text
    if (opt):
        cmd += " " + root.find("optPar").text
    try:
        cmdExec = cmd.format(input=inputFile)
    except KeyError:
        print (bcolors.FAIL+"[ERROR] Oops! Wrong parameters mapping. Check your architecture file, needed parameters are {input}.")
        sys.exit(1)
    print (bcolors.ENDC+"[EXEC] " + cmdExec)