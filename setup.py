import os
import subprocess
from subprocess import call
import settings
from settings import *

def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

print ("*********** MR Explorator - By Umberto Ferrandino ***********")
print ("***********  email: contact@umbertoferrandino.it  ***********")
print ("**********************  SETUP PROGRAM  **********************")
print ("\n")

print "Checking directory structure...",
if (not os.path.isdir(archsPath)):
    print "\nCreating the archs directory...",
    os.mkdir(archsPath)
    print "Done",
if (not os.path.isdir(toolsetPath)):
    print "\nCreating the toolset directory...",
    os.mkdir(toolsetPath)
    print "Done",

print("\nChecking for common toolsets (ABC, Yosys)...")

######## ABC

resp = raw_input("Is ABC from Berkeley installed? (y/n): ")
if (resp.lower()=="y" or resp.lower() == "yes"):
    print("Searching for abc binary in PATH...")
    abc_found = False
    abc_path = "abc"
    while (not abc_found):
        if which(abc_path) == None:
            abc_path = raw_input("ABC not found. Please specify the path for abc binary file: ")
        else: 
            abc_found = True
        # try:
        #     devnull = open(os.devnull, 'w')
        #     subprocess.Popen([abc_path], stdout=devnull, stderr=devnull).communicate()
        # except OSError as e:
        #     if e.errno == os.errno.ENOENT:
        #         abc_path = raw_input("ABC not found. Please specify the path for abc binary file: ")
        # abc_found = True

    print("Configuring default ABC conversion paths...")
    print "Creating the abc toolset directory...",
    if (not os.path.isdir(toolsetPath+'/'+'abc')):
        os.mkdir(toolsetPath+'/'+'abc')
        print "Done",
    print "\nCreating the abc conversion paths directory...",
    if (not os.path.isdir(toolsetPath+'/'+'abc/cp')):
        os.mkdir(toolsetPath+'/'+'abc/cp')
        print "Done",
    print "\nCreating the abc default configuration file...",
    if (not os.path.exists(toolsetPath+'/'+'abc/abc.cfg')):
        f = open(toolsetPath+'/'+'abc/abc.cfg','w')
        f.write("tool_path:"+abc_path+"\n"+"conversion_paths:v-blif,blif-v,eqn-blif,blif-eqn\n"+"command_parameters:-tblif -f {command} -o {output} {input}")
        f.close()
        print "Done",
    print "\nCreating the abc default conversion path files..."
    # if (not os.path.exists(toolsetPath+'/'+'abc/cp/v-blif')):
else:
    print("[WARNING] MR Explorator could not be able to convert some file types without ABC. Think about installing ABC and run again the setup.")

######## Yosys

resp = raw_input("Is Yosys installed? (y/n): ")
if (resp.lower()=="y" or resp.lower() == "yes"):
    print("Searching for yosys binary in PATH...")
    yosys_found = False
    yosys_path = "yosys"
    while (not yosys_found):
        if which(yosys_path) == None:
            yosys_path = raw_input("Yosys not found. Please specify the path for yosys binary file: ")
        else:     
            yosys_found = True
        # try:
        #     devnull = open(os.devnull, 'w')
        #     subprocess.Popen([yosys_path], stdout=devnull, stderr=devnull)
        # except OSError as e:
        #     if e.errno == os.errno.ENOENT:
        #         yosys_path = raw_input("Yosys not found. Please specify the path for yosys binary file: ")
        # yosys_found = True
    print("Configuring default Yosys conversion paths...")

    print "Creating the Yosys toolset directory...",
    if (not os.path.isdir(toolsetPath+'/'+'yosys')):
        os.mkdir(toolsetPath+'/'+'yosys')
        print "Done",
    print "\nCreating the Yosys conversion paths directory...",
    if (not os.path.isdir(toolsetPath+'/'+'yosys/cp')):
        os.mkdir(toolsetPath+'/'+'yosys/cp')
        print "Done",
    print "\nCreating the Yosys default configuration file...",
    if (not os.path.exists(toolsetPath+'/'+'yosys/yosys.cfg')):
        f = open(toolsetPath+'/'+'yosys/yosys.cfg','w')
        f.write("tool_path:"+yosys_path+"\n"+"conversion_paths:v-blif,blif-v,eqn-blif,blif-eqn\n"+"command_parameters:-tblif -f {command} -o {output} {input}")
        f.close()
        print "Done",
    print "\nCreating the Yosys default conversion path files..."

else:
    print("[WARNING] MR Explorator could not be able to convert some file types without Yosys. Think about installing Yosys and run again the setup.")

print "[OK] - Setup completed succesfully!"